﻿namespace Assets.Scripts
{
    public enum EnumDirection
    {
        All,
        Up,
        Down,
        Left,
        Right
    }
}
