﻿using UnityEngine;

namespace Assets.Scripts.SaveManager
{
    [System.Serializable]
    public class SavedOptions
    {
        public float MasterSound;
        public float SfxSound;
        public float MusicSound;
    }
}
