﻿using Assets.Scripts.Food;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public class FoodButton : MonoBehaviour
    {
        public GameObject Food;
        public bool IsActive = false;
    }
}
