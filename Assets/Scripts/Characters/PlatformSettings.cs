﻿using UnityEngine;

namespace Assets.Scripts.Characters
{
    class PlatformSettings : MonoBehaviour
    {
        public EnumDirection DirectionLimited;

        public GameObject LifterOrDropperTarget;
        public bool ChangeDirection;
    }
}
