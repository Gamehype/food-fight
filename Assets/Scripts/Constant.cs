﻿namespace Assets.Scripts
{
    public static class Constant
    {
        public const string PlatformLimiter = "Platform Limiter";
        public const string PlatformDropper = "Platform Dropper";
        public const string PlatformLifter = "Platform Lifter";
        public const string FoodButton = "Food Button"; 
        public const string Floor = "Floor"; 
        public const string Fragment = "Fragment"; 
        public const string SousChef = "Sous Chef"; 
        public const string Untagged = "Untagged";
		public const string SelectedFood = "SelectedFood";
		public const string DontDestroyOnLoad = "DontDestroyOnLoad";
		public const string Bouncy = "Bouncy";
		public const string Sticky = "Sticky";
        public const string Modal = "Modal";
        public const int Ennemy = 9;
        public const int Player = 8;
        public const int Default = 0;
    }
}
