﻿namespace Assets.Scripts.Food
{
    public enum EnumFood
    {
        Blowfish,
        Blueberry,
        Chicken,
        Coconut,
        FlambeePudding,
        HotPepper,
        Jello,
        Pomegranate,
        Strawberry,
    }
}
