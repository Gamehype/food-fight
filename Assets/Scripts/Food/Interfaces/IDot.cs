﻿namespace Assets.Scripts.Food.Interfaces
{
    public interface IDot
    {
        int DotDamage { get; set; }
        int DotTimer { get; set; }
    }
}