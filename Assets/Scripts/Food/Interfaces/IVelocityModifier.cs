﻿namespace Assets.Scripts.Food.Interfaces
{
    public interface IVelocityModifier
    {
        float VelocityModifier { get; set; }
    }
}