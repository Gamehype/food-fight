﻿namespace Assets.Scripts.Canon
{
    public enum EnumCanonState
    {
        Idle,
        PowerbarMoving,
        Launched,
        SecondAbility,
        FollowFragment,
        ReturnToCanon
    }
}
